#include <string>
#include <vector>
#include <random>

using std::string;
using namespace std;

string randDNA(int seed, string bases, int n)
{
	random_device rd;
	mt19937 eng1(seed) ;
	uniform_int_distribution <> distribution (0, bases.size() -1);
	

	string rndstr;

	for(auto i=0 ; i < n ; i++)
	{
		rndstr += bases[distribution(eng1)];
	}

	return rndstr;
}
